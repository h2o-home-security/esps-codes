#include <Arduino.h>
/*
 * author:
 * date: 20 Jan 2018
*/
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiUdp.h>
#include <HTTPClient.h>
#include <WebServer.h>
#include <mDNS.h>
#include <time.h>
#include <OneWire.h>            // Download the Library
#include <DallasTemperature.h>  // Download the Library

#define DEBUG

// If Needed
#define DISABLE_THINGS_SPEAK true

#define RED_LED         4
#define GREEN_LED       2
#define H_BRIDGE_A      22
#define H_BRIDGE_B      23
#define TOGGLE_SOLENOID  0
#define ONE_WIRE_BUS 18      // DS18D20

#define TEMPTrigger 35    // temperature in Celsius

/* ThingSpeak API KEY */
const String thingSpeakKey = "***************"; // mine
String expoToken = "ExponentPushToken[Xn2z0uLdTcdpXtu2JLapcb]";

#ifndef STASSID
#define STASSID "NETGEAR00"    // WiFi SSID
#define STAPSK  "****************"    // WiFi Password
#endif

WebServer server(80);
// Setup a oneWire instance to communicate with any OneWire device
OneWire oneWire(ONE_WIRE_BUS);	
// Pass oneWire reference to DallasTemperature library
DallasTemperature sensors(&oneWire);

const char* rootCACertificate =  // of Expo.host  for Notifications
R"EOF(-----BEGIN CERTIFICATE-----
MIIDSjCCAjKgAwIBAgIQRK+wgNajJ7qJMDmGLvhAazANBgkqhkiG9w0BAQUFADA/
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
DkRTVCBSb290IENBIFgzMB4XDTAwMDkzMDIxMTIxOVoXDTIxMDkzMDE0MDExNVow
PzEkMCIGA1UEChMbRGlnaXRhbCBTaWduYXR1cmUgVHJ1c3QgQ28uMRcwFQYDVQQD
Ew5EU1QgUm9vdCBDQSBYMzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
AN+v6ZdQCINXtMxiZfaQguzH0yxrMMpb7NnDfcdAwRgUi+DoM3ZJKuM/IUmTrE4O
rz5Iy2Xu/NMhD2XSKtkyj4zl93ewEnu1lcCJo6m67XMuegwGMoOifooUMM0RoOEq
OLl5CjH9UL2AZd+3UWODyOKIYepLYYHsUmu5ouJLGiifSKOeDNoJjj4XLh7dIN9b
xiqKqy69cK3FCxolkHRyxXtqqzTWMIn/5WgTe1QLyNau7Fqckh49ZLOMxt+/yUFw
7BZy1SbsOFU5Q9D8/RhcQPGX69Wam40dutolucbY38EVAjqr2m7xPi71XAicPNaD
aeQQmxkqtilX4+U9m5/wAl0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNV
HQ8BAf8EBAMCAQYwHQYDVR0OBBYEFMSnsaR7LHH62+FLkHX/xBVghYkQMA0GCSqG
SIb3DQEBBQUAA4IBAQCjGiybFwBcqR7uKGY3Or+Dxz9LwwmglSBd49lZRNI+DT69
ikugdB/OEIKcdBodfpga3csTS7MgROSR6cz8faXbauX+5v3gTt23ADq1cEmv8uXr
AvHRAosZy5Q6XkjEGB5YGV8eAlrwDPGxrancWYaLbumR9YbK+rlmM6pZW87ipxZz
R8srzJmwN0jP41ZL9c8PDHIyh8bwRLtTcm1D9SZImlJnt1ir/md2cXjbDaJWFBM5
JDGFoqgCWjBH4d1QB7wCCZAA62RjYJsWvIjJEubSfZGL+T0yjWW06XyxV3bqxbYo
Ob8VZRzI9neWagqNdwvYkQsEjgfbKbYK7p2CNTUQ
-----END CERTIFICATE-----)EOF"
;

// Not sure if WiFiClientSecure checks the validity date of the certificate. 
// Setting clock just to be sure...
void setClock() {
  // Configure the Time UTC+3
  configTime( 60*60*3, 0, "pool.ntp.org", "time.nist.gov");
  #ifdef DEBUG
  Serial.print(F("Waiting for NTP time sync: "));
  #endif
  time_t nowSecs = time(nullptr);
  while (nowSecs < 8 * 3600 * 2) {
    delay(500);
    #ifdef DEBUG
    Serial.print(F("."));
    #endif
    yield();
    nowSecs = time(nullptr);
  }
  struct tm timeinfo;
  gmtime_r(&nowSecs, &timeinfo);
  #ifdef DEBUG
  Serial.println();
  Serial.print(F("Current time: "));
  Serial.print(asctime(&timeinfo));
  #endif
}


bool thingSpeak(int sensorNo, int solenoidState){
  if(DISABLE_THINGS_SPEAK){
    return true;
  }
  // Use WiFiClient class to create TCP connections
      WiFiClient client;
      
      if (!client.connect("api.thingspeak.com", 80)) {
  #ifdef DEBUG
        Serial.println("connection failed");
  #endif
        return false;
      }
      else {
        // This will send a string to the server
  #ifdef DEBUG
        Serial.println("Sending to ThingSpeak");
  #endif
        if (client.connected()) {
          String postStr = "";
          
          postStr += thingSpeakKey;
          if(sensorNo){
            postStr += "&field";
            postStr += String(sensorNo);
            postStr += "=1";
          }
          postStr += "&field8=" + String(solenoidState) + "\r\n\r\n";  // insert 0 to solenoid chart
      
          client.print("POST /update HTTP/1.1\n");
          client.print("Host: api.thingspeak.com\n");
          client.print("Connection: close\n");
          client.print("X-THINGSPEAKAPIKEY: " + thingSpeakKey + "\n");
          client.print("Content-Type: application/x-www-form-urlencoded\n");
          client.print("Content-Length: ");
          client.print(postStr.length());
          client.print("\n\n");
          client.print(postStr);
  
        }
        else {
  #ifdef DEBUG
          Serial.println("connection failed");
  #endif
          return false;
        }
      
        // wait for data to be available
        unsigned long timeout = millis();
        while (client.available() == 0) {
          if (millis() - timeout > 30000) {
  #ifdef DEBUG
            Serial.println(">>> Client Timeout !");
  #endif
            client.stop();
            return false;
          }
        }
      
        // Read all the lines of the reply from server and print them to Serial
  #ifdef DEBUG
        Serial.println("receiving from remote server");
  #endif
        // not testing 'client.connected()' since we do not need to send data here
        while (client.available()) {
          char ch = static_cast<char>(client.read());
  #ifdef DEBUG
          Serial.print(ch);
  #endif
        }
      
        // Close the connection
  #ifdef DEBUG
        Serial.println();
        Serial.println("closing connection");
  #endif
        client.stop();  
      }
      return true;
}


String getFullFormattedTime() {
   time_t rawtime = time(nullptr);
   struct tm * ti;
   ti = localtime(&rawtime);

   uint16_t year = ti->tm_year + 1900;
   String yearStr = String(year);

   uint8_t month = ti->tm_mon + 1;
   String monthStr = month < 10 ? "0" + String(month) : String(month);

   uint8_t day = ti->tm_mday;
   String dayStr = day < 10 ? "0" + String(day) : String(day);

   uint8_t hours = ti->tm_hour;
   String hoursStr = hours < 10 ? "0" + String(hours) : String(hours);

   uint8_t minutes = ti->tm_min;
   String minuteStr = minutes < 10 ? "0" + String(minutes) : String(minutes);

   uint8_t seconds = ti->tm_sec;
   String secondStr = seconds < 10 ? "0" + String(seconds) : String(seconds);

   return yearStr + "-" + monthStr + "-" + dayStr + " " +
          hoursStr + ":" + minuteStr + ":" + secondStr;
}


bool pushNotificationExpo(String to, String title, String body, String data){
  bool ret = false;
  WiFiClientSecure client;
  client.setCACert(rootCACertificate);
/*
  const char* payload = R"--({
    "to": "ExponentPushToken[Xn2z0uLdTcdpXtu2JLapcb]",
    "title": "Hello",
    "body": "Man",
    "sound": "default",
    "data": {
        "msg": "help"
    }
})--";
*/
  String payload = "{ \"to\":\"" + to + "\", ";
  payload += "\"title\":\"" + title + "\", ";
  payload += "\"body\":\"" + body + "\", ";
  payload += "\"data\":" + data + " }";
  #ifdef DEBUG
  Serial.print("Post Message:");
  Serial.println(payload);
  #endif
      {
      // Add a scoping block for HTTPClient https to make sure it is destroyed before WiFiClientSecure *client is 
      HTTPClient https;
      #ifdef DEBUG
      Serial.print("[HTTPS] begin...\n");
      #endif
      if (https.begin(client, "https://exp.host/--/api/v2/push/send")) {  // HTTPS
        #ifdef DEBUG
        Serial.print("[HTTPS] POST...\n");
        #endif
        // start connection and send HTTP header
        https.addHeader("Content-Type", "application/json"); //Specify content-type header
        int httpCode = https.POST(payload);
        
        // httpCode will be negative on error
        if (httpCode > 0) {
          
          // HTTP header has been send and Server response header has been handled
          #ifdef DEBUG
          Serial.printf("[HTTPS] POST... code: %d\n", httpCode);
          #endif
          // file found at server
          if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
            String payload = https.getString();
            #ifdef DEBUG
            Serial.println(payload);
            #endif
            ret = true;
          }
          
        } else {
          #ifdef DEBUG
          Serial.printf("[HTTPS] POST... failed, error: %s\n", https.errorToString(httpCode).c_str());
          #endif
        }
        https.end();
      } else {
        #ifdef DEBUG
        Serial.printf("[HTTPS] Unable to connect\n");
        #endif
      }

      // End extra scoping block
    }
    return ret;
}


int solenoidState = 1;

void blinkLed(int no) {
  for(int i = 0; i < no; i++) {
    digitalWrite(GREEN_LED, HIGH);
    delay(100);
    digitalWrite(GREEN_LED, LOW);
    delay(100);
  }
}

/* to close solenoid, HBridge A driven HIGH, HBridge B driven LOW for 30 milliseconds, then A and B driven LOW*/
void closeSolenoid(void) {
  #ifdef DEBUG
    Serial.println("Solenoid Closed");
  #endif
  digitalWrite(H_BRIDGE_A, LOW);
  digitalWrite(H_BRIDGE_B, HIGH);
  delay(30);
  digitalWrite(H_BRIDGE_A, LOW);
  digitalWrite(H_BRIDGE_B, LOW);
  
  digitalWrite(RED_LED, HIGH);
  digitalWrite(GREEN_LED, LOW);
  solenoidState = 0;
}

/* to close solenoid, HBridge A driven HIGH, HBridge B driven LOW for 30 milliseconds, then A and B driven LOW*/
void openSolenoid(void) {
  #ifdef DEBUG
    Serial.println("Solenoid Opened");
  #endif
  digitalWrite(H_BRIDGE_A, HIGH);
  digitalWrite(H_BRIDGE_B, LOW);
  delay(30);
  digitalWrite(H_BRIDGE_A, LOW);
  digitalWrite(H_BRIDGE_B, LOW);
  
  digitalWrite(RED_LED, LOW);
  digitalWrite(GREEN_LED, HIGH);
  solenoidState = 1;
}

void handleRoot() {
  bool parameterFound = false;
  int triggered = 0;
  int sensorNo = 0;
  //digitalWrite(led, LOW);  /* turn on led */

  String message = "";
  
  if (server.hasArg("sensor")) {     //Parameter found
    message += "Sensor Argument = ";
    message += server.arg("sensor");     //Gets the value of the query parameter
    
    sensorNo = server.arg("sensor").toInt();
  }

  if (server.hasArg("triggered")) {     //Parameter found
    message += "\nTriggered Argument = ";
    message += server.arg("triggered");     //Gets the value of the query parameter
    
    triggered = server.arg("triggered").toInt();
    parameterFound = true;
  }

#ifdef DEBUG    
    Serial.println("Received Parameters Client:");
    Serial.println(message);
#endif
  
  if(!parameterFound) {     //Parameter not found
    server.send(404, "text/plain", "Bad Parameter!");
    //digitalWrite(led, HIGH);  /* turn off led */
    return;
  }else {
    /* write to nano with sensor info */
    /* close solenoid if Trigger value is 1, otherwise, Do nothing */

    if( (triggered == 1) ){
      closeSolenoid();
      if(sensorNo){
        // Blink for 5 Times indicate a trigger Event from ESP-7
         blinkLed(5);
        // Send Notification with Sensor NO
        pushNotificationExpo(expoToken,"Sensor Triggered","Sensor " + String(sensorNo) + " Triggered and Solenoid Closed at " + getFullFormattedTime() ,"{}");
      }

      if(!thingSpeak(sensorNo, 0)){
        server.send(404, "text/plain", "ThingSpeak Connection Failed");          //Returns the HTTP response
        //digitalWrite(led, HIGH);  /* turn off led */
        return;
      }
    }else{
      openSolenoid();
      if(sensorNo){
        // Send Notification with Sensor NO #TODO
        pushNotificationExpo(expoToken,"Sensor Released","Sensor " + String(sensorNo) + " Released and Solenoid Opened at " + getFullFormattedTime() ,"{}");
      }
      if(!thingSpeak(0, 1)){
        server.send(404, "text/plain", "ThingSpeak Connection Failed");          //Returns the HTTP response
        //digitalWrite(led, HIGH);  /* turn off led */
        return;
      }
    }
  }
  server.send(200, "text/plain", message);          //Returns the HTTP response
  //digitalWrite(led, HIGH);  /* turn off led */
}

void handleNotFound(){
  //digitalWrite(led, LOW);  /* turn on led */
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  //digitalWrite(led, HIGH);  /* turn off led */
}

void setup(void){
  
  Serial.begin(115200);

  Serial.println("Start");
  Serial.setDebugOutput(true);
  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);

  pinMode(H_BRIDGE_A, OUTPUT);
  pinMode(H_BRIDGE_B, OUTPUT);

  pinMode(TOGGLE_SOLENOID, INPUT_PULLUP);

  digitalWrite(RED_LED, LOW);
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(H_BRIDGE_A, LOW);
  digitalWrite(H_BRIDGE_B, LOW);

  blinkLed(3);

  sensors.begin();	// Start up the library

  WiFi.begin(STASSID, STAPSK);
#ifdef DEBUG
  Serial.println("");
  Serial.print("Trying to connect to ");
  Serial.println(STASSID);
#endif
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
#ifdef DEBUG
    Serial.print(".");
#endif
  }
#ifdef DEBUG
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(STASSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
#endif
  /*if (MDNS.begin("esp8266")) {
#ifdef DEBUG
    Serial.println("MDNS responder started");
#endif
  }*/

  setClock(); 

  server.on("/", handleRoot);
  server.on("/temp", [](){
    sensors.requestTemperatures(); 
    float Temperature = sensors.getTempCByIndex(0);
    String json = "{ \"Temp\" : " + String(Temperature) + "}";
    server.send(200, "application/json", json);
  });
  server.onNotFound(handleNotFound);

  server.begin();
#ifdef DEBUG
  Serial.println("HTTP server started");
#endif
}

int tempupdate = 0;

void loop(void){
  
  if(millis() - tempupdate > 1000){
    tempupdate = millis();
    sensors.requestTemperatures(); 
    float Temperature = sensors.getTempCByIndex(0);
#ifdef DEBUG
    //print the temperature in Celsius
    Serial.print("Temperature: ");
    Serial.println(Temperature);
#endif
    if(Temperature > TEMPTrigger && solenoidState == 1){
      #ifdef DEBUG
        Serial.print("HIGH Temp >>>> ");
      #endif
      closeSolenoid();
      pushNotificationExpo(expoToken,"Solenoid Closed","Temperature is High and Solenoid Closed at " + getFullFormattedTime() ,"{}");
      thingSpeak(0, 0); // report to the cloud Soleniod Status only
    }

  }
  // Reset Button Part
  if(digitalRead(TOGGLE_SOLENOID) == LOW) {
    /* delay for debounce */
    delay(100);
    #ifdef DEBUG
    //
    Serial.print("User Manual Trigger");
    #endif
    while(digitalRead(TOGGLE_SOLENOID) == LOW);
    if(solenoidState == 0) {
      openSolenoid();
      pushNotificationExpo(expoToken,"Solenoid Opened","User Opened the Solenoid at " + getFullFormattedTime() ,"{}");
      thingSpeak(0, 1); // report to the cloud Soleniod Status only
    }
    else {
      closeSolenoid();
      pushNotificationExpo(expoToken,"Solenoid Closed","User Closed the Solenoid at " + getFullFormattedTime() ,"{}");
      thingSpeak(0, 0); // report to the cloud Soleniod Status only
    }
  }
  
  server.handleClient();
  
}
