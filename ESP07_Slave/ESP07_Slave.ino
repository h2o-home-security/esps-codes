#include <Arduino.h>
/*
 * author: 
 * date: 20 Jan 2018
*/

extern "C" {
  #include "gpio.h"
}

extern "C" {
  #include "user_interface.h"
}

#include <ESP8266WiFi.h>

#ifndef STASSID
#define STASSID "NETGEAR00"    // WiFi SSID
#define STAPSK  "****************"    // WiFi Password
#endif

#define MASTER_SERVER_IP      "192.168.43.154"   // Master Local Ip. (Ex. 192.168.1.26)
#define MASTER_SERVER_PORT    (80)

const int sensorNo = 1;
const char* ssid     = STASSID;
const char* password = STAPSK;

const char* host = MASTER_SERVER_IP;
const uint16_t port = MASTER_SERVER_PORT;

#define ledPin      D1
#define probePin    0
#define holdPin     14

int sensorTriggered = false;
int masterResponseOk = false;
bool wifiConnected = false;

#define SENSELEVEL HIGH

void blinkLed(int no) {
  for(int i = 0; i < no; i++) {
    digitalWrite(ledPin, HIGH);
    delay(200);
    digitalWrite(ledPin, LOW);
    delay(200);
  }
}
void setup() {
  //gpio_init();

  pinMode(holdPin, OUTPUT);
  digitalWrite(holdPin, HIGH);
  
  Serial.begin(115200);
  Serial.println("\nStart");
  //delay(1000);
  pinMode(probePin, INPUT);
  pinMode(ledPin, OUTPUT);
  blinkLed(3);

}

int sendTriggerResult(int triggered) {
  int err_counter = 0;

  if(WiFi.status() != WL_CONNECTED)
  {
     // We start by connecting to a WiFi network
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
  
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
  
    while (WiFi.status() != WL_CONNECTED) {
      //blinkLed
      digitalWrite(ledPin, HIGH);
      delay(200);
      digitalWrite(ledPin, LOW);
      delay(200);

      Serial.print(".");
      if(err_counter++ == 20) { /* 20 sec. timeout */
        Serial.print("Couldn't Connect to: ");
        Serial.println(ssid);
        return -1;
      }
    }

    Serial.print("Wifi Status: ");
    Serial.println(WiFi.status());
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
  
  Serial.print("connecting to server");
  Serial.print(host);
  Serial.print(':');
  Serial.println(port);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    return -1;
  }

  // This will send a string to the server
  Serial.print("sending data to the server:");
  Serial.println(host);
  if (client.connected()) {

    String postStr = "GET /?sensor=";
    postStr += sensorNo;
    postStr += "&triggered=";
    postStr += triggered;

    client.print(postStr);
    client.print(" HTTP/1.1\r\n");
    client.print("Host: ");
    client.print(MASTER_SERVER_IP);
    client.print("\r\nConnection: close\r\n\r\n");
  }

  // wait for data to be available
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 20000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return -1;
    }
  }

  // Read all the lines of the reply from server and print them to Serial
  Serial.println("receiving from remote server");
  // not testing 'client.connected()' since we do not need to send data here
  String masterResponse = "";
  while (client.available()) {
    char ch = static_cast<char>(client.read());
    masterResponse += ch;
  }

  Serial.println("Master Response:");
  Serial.println(masterResponse);

  // Close the connection
  Serial.println();
  Serial.println("closing connection");
  client.stop();

  if(masterResponse.indexOf("200 OK") >= 0)  // master returns "200 OK" if cloud push is OK
    return 0;

  return -1;
}


GPIO_INT_TYPE triggerEdge = GPIO_PIN_INTR_HILEVEL;

// the loop function runs over and over again forever
void loop() {
  // static char wokeUp = false;
  int pinVal;
  
  pinVal = digitalRead(probePin);
  if(pinVal == SENSELEVEL) {
    delay(100);  // wait for debounce
    pinVal = digitalRead(probePin);
    if(pinVal == SENSELEVEL) {
      Serial.println("Liquid Detected.");
      digitalWrite(ledPin, HIGH);
      blinkLed(5);
      sensorTriggered = true;

      if(!masterResponseOk) {
        if(sendTriggerResult(1) == 0) {  // if master response is OK, this func returns 0, otherwise it tries to send to master until it returns OK and while probe is triggered
          masterResponseOk = true; 
        }
      }
      triggerEdge = GPIO_PIN_INTR_LOLEVEL;
    }
  }
  else if(pinVal == !SENSELEVEL && sensorTriggered){
    delay(100);  // wait for debounce
    pinVal = digitalRead(probePin);
    if(pinVal == !SENSELEVEL) {
      masterResponseOk = false;
      sensorTriggered = false;
      Serial.println("Water Sense Removed.");
      //sendTriggerResult(0);  // we are not sending this event to master
      triggerEdge = GPIO_PIN_INTR_HILEVEL;
      // set device sleep 
      Serial.println("Sleeping...");
      digitalWrite(holdPin, LOW);
    }
  }
}
